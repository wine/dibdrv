/*
 * DIBDRV depth specific DIB rendering functions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dib_render.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* DIB_GetNearestIndex: Return closest matching color table entry. */
INT DIB_GetNearestIndex( DIBDRVBITMAP *bmp, BYTE r, BYTE g, BYTE b )
{
    INT i, diff, bestdiff = -1, ret = 0;
    for (i = 0; i < bmp->nb_colors; i++)
    {
        diff = (bmp->color_table[i].rgbBlue - b)*(bmp->color_table[i].rgbBlue - b) +
               (bmp->color_table[i].rgbGreen - g)*(bmp->color_table[i].rgbGreen - g) +
               (bmp->color_table[i].rgbRed - r)*(bmp->color_table[i].rgbRed - r);
        if (!diff)
            return i;
        if (bestdiff == -1 || diff < bestdiff)
        {
            ret = i;
            bestdiff = diff;
        }
    }
    return ret;
}

/* Public Functions
 *
 * AdvancePosition(): Move drawing location relative to current position by the requested number
 * of pixels. It is the caller's responsibility to traverse scan lines correctly, as this does
 * not check for that.
 *
 * Copy(): Copy the requested number of pixels from source DIB to destination DIB performing format
 * conversions as necessary. Since this requires two images, call the Copy() member of the
 * destination image, pass the source conversion function table, and copy translates the call into
 * the appropriate conversion routine. The caller is responsible to traverse scan lines
 * correctly.
 *
 * MoveXY(): Move drawing location using X,Y coordinates. Stores address with whatever shifting
 * required to correct value. Returns a BOOL where FALSE means an attempt to move out of range.
 *
 * ReadColor(): Read pixel and translate into color components.
 *
 * SelectColor(): Store a color encoded for the DIB.
 *
 * SetPixel(): Optimized SetPixel for GDI by writing a COLORREF directly.
 *
 * WriteColor(): Write a number of pixels with stored color.
 */

void DIB1_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->shift -= n;
    while (bmp->shift < 0)
    {
        bmp->addr = (BYTE*)bmp->addr + 1;
        bmp->shift += 8;
    }
}

void DIB1_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB1( dst, src, num_pixels );
}

void DIB1_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int r = (unsigned int)bmp->bits + y*bmp->widthbytes + x/8; /* 1 byte per 8 pixels */
    bmp->shift = 7 - x % 8; /* maximum shift within unsigned char: 7 */
    bmp->addr = (void *)r;
}

void DIB1_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB_GetColorTableEntry( bmp, (*(BYTE *)bmp->addr >> bmp->shift) & 1, r, g, b );
}

/* DIB1_SelectColor ----> DIB8_SelectColor */

void DIB1_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    BYTE *p = bmp->addr;
    *p = DIB_GetNearestIndex( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) ) ?
         *p | 1 << bmp->shift : *p & ~(1 << bmp->shift);
}

void DIB1_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        BYTE *p = bmp->addr;
        *p = bmp->color8 ? *p | 1 << bmp->shift : *p & ~(1 << bmp->shift);
        DIB1_AdvancePosition( bmp, 1 );
    }
}

void DIB4_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->shift -= n;
    while (bmp->shift < 0)
    {
        bmp->addr = (BYTE*)bmp->addr + 1;
        bmp->shift += 2;
    }
}

void DIB4_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB4( dst, src, num_pixels );
}

void DIB4_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int r = (unsigned int)bmp->bits + y*bmp->widthbytes + x/2; /* 1 byte per 2 pixels */
    bmp->shift = x % 2; /* If true, we need the first 4 bits */
    bmp->addr = (void *)r;
}

void DIB4_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB_GetColorTableEntry( bmp,
                               bmp->shift ? *(BYTE *)bmp->addr & 0xf : *(BYTE *)bmp->addr >> 4,
                               r, g, b );
}

/* DIB4_SelectColor ----> DIB8_SelectColor */

void DIB4_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    BYTE *p = bmp->addr;
    *p = bmp->shift ?
         (*p & 0xf0) |
         DIB_GetNearestIndex( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) ) :
         (*p & 0xf ) |
         (DIB_GetNearestIndex( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) ) << 4);
}

void DIB4_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        BYTE *p = bmp->addr;
        *p = bmp->shift ? (*p & 0xf0)|bmp->color8 : (*p & 0xf)|(bmp->color8 << 4);
        DIB4_AdvancePosition( bmp, 1 );
    }
}

void DIB8_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->addr = (BYTE*)bmp->addr + n;
}

void DIB8_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB8( dst, src, num_pixels );
}

void DIB8_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int r = (unsigned int)bmp->bits + y*bmp->widthbytes + x; /* 1 byte per 1 pixel */
    bmp->addr = (void *)r;
}

void DIB8_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB_GetColorTableEntry( bmp, *(BYTE *)bmp->addr, r, g, b );
}

void DIB8_SelectColor( DIBDRVBITMAP *bmp, COLORREF c )
{
    bmp->color8 = DIB_GetNearestIndex( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB8_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    *(BYTE *)bmp->addr = DIB_GetNearestIndex( bmp,
                         GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB8_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        *(BYTE *)bmp->addr = bmp->color8;
        DIB8_AdvancePosition( bmp, 1 );
    }
}

void DIB16_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->addr = (WORD*)bmp->addr + n;
}

void DIB16_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB16( dst, src, num_pixels );
}

void DIB16_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int addr = (unsigned int)bmp->bits + y*bmp->widthbytes + x*2; /* 2 bytes per pixel */
    bmp->addr = (void *)addr;
}

void DIB16_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB16_Decode( bmp, *(WORD*)bmp->addr, r, g, b );
}

void DIB16_SelectColor( DIBDRVBITMAP *bmp, COLORREF c )
{
    bmp->color16 = DIB16_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB16_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    *(WORD *)bmp->addr = DIB16_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB16_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        *(WORD *)bmp->addr = bmp->color16;
        DIB16_AdvancePosition( bmp, 1 );
    }
}

void DIB24_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->addr = (RGBTRIPLE*)bmp->addr + n;
}

void DIB24_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB24( dst, src, num_pixels );
}

void DIB24_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int addr = (unsigned int)bmp->bits + y*bmp->widthbytes + x*3; /* 3 bytes per pixel */
    bmp->addr = (void *)addr;
}

void DIB24_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB24_Decode( bmp, *(RGBTRIPLE*)bmp->addr, r, g, b );
}

void DIB24_SelectColor( DIBDRVBITMAP *bmp, COLORREF c )
{
    bmp->color24 = DIB24_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB24_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    RGBTRIPLE *p = bmp->addr;
    *p = DIB24_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB24_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *p = bmp->addr;
        *p = bmp->color24;
        DIB24_AdvancePosition( bmp, 1 );
    }
}

void DIB32_AdvancePosition( DIBDRVBITMAP *bmp, int n )
{
    bmp->addr = (DWORD*)bmp->addr + n;
}

void DIB32_Copy( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels, const DIBCONVERT *convs )
{
    convs->ConvertToDIB32( dst, src, num_pixels );
}

void DIB32_MoveXY( DIBDRVBITMAP *bmp, INT x, INT y )
{
    unsigned int addr = (unsigned int)bmp->bits + y*bmp->widthbytes + x*4; /* 4 bytes per pixel */
    bmp->addr = (void *)addr;
}

void DIB32_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b )
{
    DIB32_Decode( bmp, *(DWORD*)bmp->addr, r, g, b );
}

void DIB32_SelectColor( DIBDRVBITMAP *bmp, COLORREF c )
{
    bmp->color32 = DIB32_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB32_SetPixel( DIBDRVBITMAP *bmp, COLORREF c )
{
    *(DWORD *)bmp->addr = DIB32_Encode( bmp, GetRValue( c ), GetGValue( c ), GetBValue( c ) );
}

void DIB32_WriteColor( DIBDRVBITMAP *bmp, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        *(DWORD *)bmp->addr = bmp->color32;
        DIB32_AdvancePosition( bmp, 1 );
    }
}

const DIBFUNCS DIB1_funcs =
{
    &DIB1_AdvancePosition,
    &DIB1_Copy,
    &DIB1_MoveXY,
    &DIB1_ReadColor,
    &DIB8_SelectColor,
    &DIB1_SetPixel,
    &DIB1_WriteColor
};

const DIBFUNCS DIB4_funcs =
{
    &DIB4_AdvancePosition,
    &DIB4_Copy,
    &DIB4_MoveXY,
    &DIB4_ReadColor,
    &DIB8_SelectColor,
    &DIB4_SetPixel,
    &DIB4_WriteColor
};

const DIBFUNCS DIB8_funcs =
{
    &DIB8_AdvancePosition,
    &DIB8_Copy,
    &DIB8_MoveXY,
    &DIB8_ReadColor,
    &DIB8_SelectColor,
    &DIB8_SetPixel,
    &DIB8_WriteColor
};

const DIBFUNCS DIB16_funcs =
{
    &DIB16_AdvancePosition,
    &DIB16_Copy,
    &DIB16_MoveXY,
    &DIB16_ReadColor,
    &DIB16_SelectColor,
    &DIB16_SetPixel,
    &DIB16_WriteColor
};

const DIBFUNCS DIB24_funcs =
{
    &DIB24_AdvancePosition,
    &DIB24_Copy,
    &DIB24_MoveXY,
    &DIB24_ReadColor,
    &DIB24_SelectColor,
    &DIB24_SetPixel,
    &DIB24_WriteColor
};

const DIBFUNCS DIB32_funcs =
{
    &DIB32_AdvancePosition,
    &DIB32_Copy,
    &DIB32_MoveXY,
    &DIB32_ReadColor,
    &DIB32_SelectColor,
    &DIB32_SetPixel,
    &DIB32_WriteColor
};
