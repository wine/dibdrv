/*
 * DIB driver bitmap objects
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

DIBFORMAT BITMAP_GetFormat( WORD depth, DWORD compression )
{
    switch (depth)
    {
    case 1:
        if (compression == BI_RGB)
            return DIBFMT_DIB1;
        break;
    case 4:
        if (compression == BI_RLE4)
            WARN("RLE compression not supported yet!\n");
        else if (compression == BI_RGB)
            return DIBFMT_DIB4;
        break;
    case 8:
        if (compression == BI_RLE8)
            WARN("RLE compression not supported yet!\n");
        else if (compression == BI_RGB)
            return DIBFMT_DIB8;
        break;
    case 16:
        if (compression == BI_BITFIELDS)
            return DIBFMT_DIB16;
        else if (compression == BI_RGB)
            return DIBFMT_X1B5G5R5;
        break;
    case 24:
        if (compression == BI_RGB)
            return DIBFMT_DIB24;
        break;
    case 32:
        if (compression == BI_BITFIELDS)
            return DIBFMT_DIB32;
        else if (compression == BI_RGB)
            return DIBFMT_X8B8G8R8;
        break;
    }
    ERR("unsupported dib format: depth=%d, compression=%d\n", depth, compression);
    return DIBFMT_UNKNOWN;
}

const DIBFUNCS *BITMAP_GetDIBFuncs( DIBFORMAT format )
{
    switch(format)
    {
    case DIBFMT_DIB1: return &DIB1_funcs;
    case DIBFMT_DIB4: return &DIB4_funcs;
    case DIBFMT_DIB8: return &DIB8_funcs;
    case DIBFMT_X1B5G5R5:
    case DIBFMT_DIB16: return &DIB16_funcs;
    case DIBFMT_DIB24: return &DIB24_funcs;
    case DIBFMT_X8B8G8R8:
    case DIBFMT_DIB32: return &DIB32_funcs;
    case DIBFMT_UNKNOWN:
    default:
        ERR("no support for format %d!\n", format);
    }
    return NULL;
}

const DIBCONVERT *BITMAP_GetDIBConversions( DIBFORMAT format )
{
    switch(format)
    {
    case DIBFMT_DIB1: return &DIB1_convs;
    case DIBFMT_DIB4: return &DIB4_convs;
    case DIBFMT_DIB8: return &DIB8_convs;
    case DIBFMT_X1B5G5R5:
    case DIBFMT_DIB16: return &DIB16_convs;
    case DIBFMT_DIB24: return &DIB24_convs;
    case DIBFMT_X8B8G8R8:
    case DIBFMT_DIB32: return &DIB32_convs;
    case DIBFMT_UNKNOWN:
    default:
        ERR("no support for format %d!\n", format);
    }
    return NULL;
}

void BITMAP_CalcBitfields( DIBDRVBITMAP *bmp, WORD depth )
{
    int i, x;
    for (i = 0; i < 3; i++)
    {
        x = 0;
        while (!((bmp->bitfields[i] >> x) & 1) && (x < depth))
            x++;
        bmp->bf_pos[i] = x;
        while (((bmp->bitfields[i] >> x) & 1) && (x < depth))
            x++;
        bmp->bf_size[i] = x - bmp->bf_pos[i];
        if (bmp->bf_size[i] > 8) bmp->bf_pos[i] += 8;
        bmp->bf_shift[i] = bmp->bf_size[i] < 8 ? 8 - bmp->bf_size[i] : 0;
    }
}

/* BITMAP_Alloc: Alloc DIBDRVBITMAP and initialize to passed-in values. Note: caller is
 * responsible for proper handling of the color table.
 */
DIBDRVBITMAP *BITMAP_Alloc( DIBFORMAT format, void *bits, INT width, INT height, INT widthbytes,
                            INT size, RGBQUAD *color_table, UINT nb_colors, DWORD *bitfields )
{
    DIBDRVBITMAP *bmp = HeapAlloc( GetProcessHeap(), 0, sizeof(DIBDRVBITMAP) );
    if (!bmp) return NULL;

    TRACE("%d %p %d %d %d %p %u %p\n", format, bits, width, height, widthbytes, color_table,
          nb_colors, bitfields);

    bmp->format = format;
    bmp->bits = bits;
    bmp->width = width;
    bmp->height = height < 0 ? -height : height;
    bmp->widthbytes = widthbytes;
    bmp->size = size;
    bmp->top_down = height < 0 ? TRUE : FALSE;
    bmp->color_table = color_table;
    bmp->nb_colors = nb_colors;

    if (format == DIBFMT_DIB16 || format == DIBFMT_DIB32)
    {
        bmp->bitfields[0] = bitfields[0];
        bmp->bitfields[1] = bitfields[1];
        bmp->bitfields[2] = bitfields[2];
        BITMAP_CalcBitfields( bmp, format == DIBFMT_DIB16 ? 16 : 32 );
    }
    else if (format == DIBFMT_X1B5G5R5)
    {
        bmp->bitfields[0] = 0x7c00;
        bmp->bitfields[1] = 0x3e0;
        bmp->bitfields[2] = 0x1f;
        BITMAP_CalcBitfields( bmp, 16 );
    }
    else if (format == DIBFMT_X8B8G8R8)
    {
        bmp->bitfields[0] = 0xff0000;
        bmp->bitfields[1] = 0xff00;
        bmp->bitfields[2] = 0xff;
        BITMAP_CalcBitfields( bmp, 32 );
    }

    return bmp;
}

/***********************************************************************
 *           BITMAP_GetDIBWidthBytes
 *
 * Return the width of a DIB bitmap in bytes. DIB bitmap data is 32-bit aligned.
 * Copied from gdi32.
 */
int BITMAP_GetDIBWidthBytes( int width, int depth )
{
    int words;

    switch(depth)
    {
        case 1:  words = (width + 31) / 32; break;
        case 4:  words = (width + 7) / 8; break;
        case 8:  words = (width + 3) / 4; break;
        case 15:
        case 16: words = (width + 1) / 2; break;
        case 24: words = (width * 3 + 3)/4; break;

        default:
            WARN("(%d): Unsupported depth\n", depth );
        /* fall through */
        case 32:
                words = width;
    }
    return 4 * words;
}

DIBDRVBITMAP *BITMAP_CreateFromBmi( const BITMAPINFO *bmi, void *bits )
{
    DIBFORMAT fmt = BITMAP_GetFormat( bmi->bmiHeader.biBitCount, bmi->bmiHeader.biCompression );
    if (!fmt) return NULL;
    return BITMAP_Alloc( fmt, bits, bmi->bmiHeader.biWidth,
                         bmi->bmiHeader.biHeight,
                         BITMAP_GetDIBWidthBytes( bmi->bmiHeader.biWidth,
                                                  bmi->bmiHeader.biBitCount ),
                         bmi->bmiHeader.biSizeImage,
                         (RGBQUAD *)bmi->bmiColors,
                         bmi->bmiHeader.biClrUsed,
                         (DWORD *)bmi->bmiColors );
}

DIBDRVBITMAP *BITMAP_CreateFromHBitmap( HBITMAP hbitmap, RGBQUAD *ct, UINT nb_colors )
{
    DIBFORMAT fmt;
    DIBSECTION dib;
    if (!GetObjectW( hbitmap, sizeof(dib), &dib )) return NULL;

    /* If we get an hbitmap from any GDI call this can mean a few things:
     * 1) We got a bitmap that is really a DIB, so it's dependent on the DIB engine. This occurs
     *    during SelectBitmap for example, so we can use it then.
     * 2) We may have gotten a bitmap that was really intended for an actual device, like
     *    winex11. If this does indeed ever happen, we will have to punt back.
     * 3) The application really committed a fatal error and got DCs mixed up. We will simply have
     *    to fail.
     * So we must perform a check whether this is a DDB no matter what we think it might be.
     */
    if (!dib.dsBm.bmBits)
    {
        /* When this is a DDB, we don't handle it right now so we will print
         * an error to inform the user, and fail.
         */
        ERR("hbitmap is actually a DDB and there is no way to handle this yet\n");
        return NULL;
    }

    fmt = BITMAP_GetFormat( dib.dsBmih.biBitCount, dib.dsBmih.biCompression );
    if (!fmt) return NULL;

    return BITMAP_Alloc( fmt, dib.dsBm.bmBits, dib.dsBmih.biWidth, dib.dsBmih.biHeight,
                         dib.dsBm.bmWidthBytes, dib.dsBmih.biSizeImage, ct, nb_colors,
                         dib.dsBitfields );
}

/***********************************************************************
 *           DIBDRV_SelectBitmap
 */
HBITMAP DIBDRV_SelectBitmap( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap, RGBQUAD *color_table,
                             UINT nb_colors )
{
    DIBDRVBITMAP *bmp;
    RGBQUAD *ct;
    TRACE("%p, %p\n", physDev, hbitmap);

    if (nb_colors)
    {
        /* make a copy of the color table */
        ct = HeapAlloc( GetProcessHeap(), 0, sizeof(RGBQUAD)*nb_colors );
        if (!ct) return 0;
        memcpy( ct, color_table, sizeof(RGBQUAD)*nb_colors );
    }
    else ct = NULL;

    bmp = BITMAP_CreateFromHBitmap( hbitmap, ct, nb_colors );
    if (!bmp)
    {
        HeapFree( GetProcessHeap(), 0, ct );
        return 0;
    }

    /* Since we have a valid DIB, we can now write over the old one */
    physDev->hbitmap = hbitmap;
    if (physDev->bmp)
    {
        if (physDev->bmp->color_table)
            HeapFree( GetProcessHeap(), 0, physDev->bmp->color_table );
        HeapFree( GetProcessHeap(), 0, physDev->bmp );
    }
    physDev->bmp = bmp;
    physDev->funcs = BITMAP_GetDIBFuncs( bmp->format );
    physDev->convs = BITMAP_GetDIBConversions( bmp->format );

    return hbitmap;
}

/****************************************************************************
 *        DIBDRV_CreateBitmap
 */
BOOL DIBDRV_CreateBitmap( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap, LPVOID bmBits )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_DeleteBitmap
 */
BOOL DIBDRV_DeleteBitmap( HBITMAP hbitmap )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetBitmapBits
 */
LONG DIBDRV_GetBitmapBits( HBITMAP hbitmap, void *buffer, LONG count )
{
    FIXME("stub\n");
    return 0;
}

/******************************************************************************
 *             DIBDRV_SetBitmapBits
 */
LONG DIBDRV_SetBitmapBits( HBITMAP hbitmap, const void *bits, LONG count )
{
    FIXME("stub\n");
    return 0;
}
