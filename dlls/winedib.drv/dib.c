/*
 * DIBDRV device-independent bitmaps
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *           DIBDRV_CreateDIBSection
 */
HBITMAP DIBDRV_CreateDIBSection( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap,
                                 const BITMAPINFO *bmi, UINT usage )
{
    FIXME("stub\n");
    return hbitmap;
}

/***********************************************************************
 *           DIBDRV_GetDIBits
 */
INT DIBDRV_GetDIBits( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap, UINT startscan,
                      UINT lines, LPCVOID bits, const BITMAPINFO *info, UINT coloruse )
{
    const DIBFUNCS *dst_funcs, *src_funcs;
    const DIBCONVERT *convs;
    DIBDRVBITMAP *dst, *src;
    int i, min_width;

    TRACE("%p %p %u %u %p %p %u\n", physDev, hbitmap, startscan, lines, bits, info, coloruse);

    dst = BITMAP_CreateFromBmi( info, (void *)bits );
    if (!dst) return 0;

    src = BITMAP_CreateFromHBitmap( hbitmap, physDev->bmp->color_table, physDev->bmp->nb_colors );
    if (!src)
    {
        HeapFree( GetProcessHeap(), 0, dst );
        return 0;
    }

    convs = BITMAP_GetDIBConversions( src->format );
    dst_funcs = BITMAP_GetDIBFuncs( dst->format );
    src_funcs = BITMAP_GetDIBFuncs( src->format );
    min_width = src->width < dst->width ?  src->width : dst->width;

    /* copy a scanline at a time */
    for (i = startscan; i < startscan + lines; i++)
    {
        dst_funcs->MoveXY( dst, 0, i );
        src_funcs->MoveXY( src, 0, i );
        dst_funcs->Copy( dst, src, min_width, convs );
    }

    HeapFree( GetProcessHeap(), 0, dst );
    HeapFree( GetProcessHeap(), 0, src );
    return i - startscan;
}

/***********************************************************************
 *           DIBDRV_SetDIBColorTable
 */
UINT DIBDRV_SetDIBColorTable( DIBDRVPHYSDEV *physDev, UINT start, UINT count,
                              const RGBQUAD *colors )
{
    TRACE("%p %u %u %p\n", physDev, start, count, colors);
    if (!physDev->bmp->color_table || start + count > physDev->bmp->nb_colors)
        return 0; /* maybe */
    memcpy(physDev->bmp->color_table + start*sizeof(RGBQUAD), colors, count*sizeof(RGBQUAD));
    return count;
}

/***********************************************************************
 *           DIBDRV_SetDIBits
 */
INT DIBDRV_SetDIBits( DIBDRVPHYSDEV *physDev, HBITMAP hbitmap, UINT startscan,
                      UINT lines, LPCVOID bits, const BITMAPINFO *info, UINT coloruse )
{
    const DIBFUNCS *dst_funcs, *src_funcs;
    const DIBCONVERT *convs;
    DIBDRVBITMAP *dst, *src;
    int i, min_width;

    TRACE("%p %p %u %u %p %p %u\n", physDev, hbitmap, startscan, lines, bits, info, coloruse);

    dst = BITMAP_CreateFromHBitmap( hbitmap, physDev->bmp->color_table, physDev->bmp->nb_colors );
    if (!dst) return 0;

    src = BITMAP_CreateFromBmi( info, (void *)bits );
    if (!src)
    {
        HeapFree( GetProcessHeap(), 0, dst );
        return 0;
    }

    convs = BITMAP_GetDIBConversions( src->format );
    dst_funcs = BITMAP_GetDIBFuncs( dst->format );
    src_funcs = BITMAP_GetDIBFuncs( src->format );
    min_width = dst->width < src->width ?  dst->width : src->width;

    /* copy a scanline at a time */
    for (i = startscan; i < startscan + lines; i++)
    {
        dst_funcs->MoveXY( dst, 0, i );
        src_funcs->MoveXY( src, 0, i );
        dst_funcs->Copy( dst, src, min_width, convs );
    }

    HeapFree( GetProcessHeap(), 0, dst );
    HeapFree( GetProcessHeap(), 0, src );
    return i - startscan;
}

/*************************************************************************
 *              DIBDRV_SetDIBitsToDevice
 */
INT DIBDRV_SetDIBitsToDevice( DIBDRVPHYSDEV *physDev, INT xDest, INT yDest, DWORD cx,
                              DWORD cy, INT xSrc, INT ySrc,
                              UINT startscan, UINT lines, LPCVOID bits,
                              const BITMAPINFO *info, UINT coloruse )
{
    const DIBFUNCS *src_funcs;
    const DIBCONVERT *convs;
    DIBDRVBITMAP *src;
    INT i, min_width;

    FIXME("not finished: %p %d %d %u %u %d %d %u %u %p %p %u\n", physDev, xDest, yDest, cx, cy,
          xSrc, ySrc, startscan, lines, bits, info, coloruse);

    src = BITMAP_CreateFromBmi( info, (void *)bits );
    if (!src) return 0;

    convs = BITMAP_GetDIBConversions( src->format );
    src_funcs = BITMAP_GetDIBFuncs( src->format );
    min_width = physDev->bmp->width < src->width ?  physDev->bmp->width : src->width;

    /* copy a scanline at a time */
    for (i = startscan; i < startscan + lines; i++)
    {
        physDev->funcs->MoveXY( physDev->bmp, xDest, yDest + i );
        src_funcs->MoveXY( src, xSrc, ySrc + i );
        physDev->funcs->Copy( physDev->bmp, src, min_width, physDev->convs );
    }

    HeapFree( GetProcessHeap(), 0, src );
    return i - startscan;
}
