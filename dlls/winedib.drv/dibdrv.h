/*
 * DIB driver private definitions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WINE_DIBDRV_H
#define __WINE_DIBDRV_H

#include "wingdi.h"

typedef enum _DIBFORMAT
{
    DIBFMT_UNKNOWN          =   0,
    DIBFMT_DIB1             =   1,
    DIBFMT_DIB4             =   2,
    /*DIBFMT_DIB4RLE          =   3,*/
    DIBFMT_DIB8             =   4,
    /*DIBFMT_DIB8RLE          =   5,*/
    DIBFMT_DIB16            =   6,
    DIBFMT_DIB24            =   7,
    DIBFMT_DIB32            =   8,
    DIBFMT_X1B5G5R5         =   9,
    DIBFMT_X8B8G8R8         =  10
} DIBFORMAT;

/* DIB driver's generic bitmap structure */
typedef struct _DIBDRVBITMAP
{
    DIBFORMAT format;
    void *bits;
    INT width;
    INT height;
    INT widthbytes;
    INT size;
    BOOL top_down;
    RGBQUAD *color_table;
    UINT nb_colors;
    DWORD bitfields[3];

    /* calculated numbers for bitfields */
    INT bf_pos[3];   /* starting position of the field */
    INT bf_size[3];  /* size of the field */
    INT bf_shift[3]; /* shifting required within a COLORREF's BYTE */

    /* current drawing address */
    void *addr;
    INT shift;

    /* ready encoded colors for drawing */
    BYTE color8;
    WORD color16;
    RGBTRIPLE color24;
    DWORD color32;
} DIBDRVBITMAP;

/* DIB conversions functions */
typedef struct _DIBCONVERT
{
    void (*ConvertToDIB1)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
    void (*ConvertToDIB4)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
    void (*ConvertToDIB8)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
    void (*ConvertToDIB16)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
    void (*ConvertToDIB24)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
    void (*ConvertToDIB32)(DIBDRVBITMAP*,DIBDRVBITMAP*,int);
} DIBCONVERT;

/* DIB manipulation functions */
typedef struct _DIBFUNCS
{
    void (*AdvancePosition)(DIBDRVBITMAP*,int);
    void (*Copy)(DIBDRVBITMAP*,DIBDRVBITMAP*,int,const DIBCONVERT*);
    void (*MoveXY)(DIBDRVBITMAP*,INT,INT);
    void (*ReadColor)(DIBDRVBITMAP*,BYTE*,BYTE*,BYTE*);
    void (*SelectColor)(DIBDRVBITMAP*,COLORREF);
    void (*SetPixel)(DIBDRVBITMAP*,COLORREF);
    void (*WriteColor)(DIBDRVBITMAP*,int);
} DIBFUNCS;

/* DIB driver physical device */
typedef struct _DIBDRVPHYSDEV
{
    HDC hdc;
    HBITMAP hbitmap;
    DIBDRVBITMAP *bmp;
    const DIBFUNCS *funcs;
    const DIBCONVERT *convs;
} DIBDRVPHYSDEV;

/* bitmap handling functions */
extern DIBFORMAT BITMAP_GetFormat( WORD depth, DWORD compression );
extern const DIBFUNCS *BITMAP_GetDIBFuncs( DIBFORMAT format );
extern const DIBCONVERT *BITMAP_GetDIBConversions( DIBFORMAT format );
extern DIBDRVBITMAP *BITMAP_CreateFromBmi( const BITMAPINFO *bmi, void *bits );
extern DIBDRVBITMAP *BITMAP_CreateFromHBitmap( HBITMAP hbitmap, RGBQUAD *ct, UINT nb_colors );

/* DIB function tables for each format */
extern const DIBFUNCS DIB1_funcs, DIB4_funcs, DIB8_funcs, DIB16_funcs, DIB24_funcs, DIB32_funcs;
extern const DIBCONVERT DIB1_convs, DIB4_convs, DIB8_convs, DIB16_convs, DIB24_convs, DIB32_convs;

#endif /* __WINE_DIBDRV_H */
