/*
 * DIBDRV OpenGL functions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

int DIBDRV_ChoosePixelFormat( DIBDRVPHYSDEV *physDev,
                              const PIXELFORMATDESCRIPTOR *ppfd )
{
    FIXME("stub\n");
    return 0;
}

int DIBDRV_DescribePixelFormat( DIBDRVPHYSDEV *physDev,
                                int iPixelFormat,
                                UINT nBytes,
                                PIXELFORMATDESCRIPTOR *ppfd )
{
    FIXME("stub\n");
    return 0;
}

int DIBDRV_GetPixelFormat( DIBDRVPHYSDEV *physDev)
{
    FIXME("stub\n");
    return 0;
}

BOOL DIBDRV_SetPixelFormat( DIBDRVPHYSDEV *physDev,
                            int iPixelFormat,
                            const PIXELFORMATDESCRIPTOR *ppfd )
{
    FIXME("stub\n");
    return TRUE;
}

BOOL DIBDRV_SwapBuffers( DIBDRVPHYSDEV *physDev )
{
    FIXME("stub\n");
    return TRUE;
}
