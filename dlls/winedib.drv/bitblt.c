/*
 * DIBDRV bit-blit operations
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *           DIBDRV_StretchBlt
 */
BOOL DIBDRV_StretchBlt( DIBDRVPHYSDEV *physDevDst, INT xDst, INT yDst,
                        INT widthDst, INT heightDst,
                        DIBDRVPHYSDEV *physDevSrc, INT xSrc, INT ySrc,
                        INT widthSrc, INT heightSrc, DWORD rop )
{
    INT i, min_width, min_height;
    TRACE("stub %p %d %d %d %d %p %d %d %d %d %d\n", physDevDst, xDst, yDst, widthDst, heightDst,
          physDevSrc, xSrc, ySrc, widthSrc, heightSrc, rop);
    min_width = physDevDst->bmp->width - yDst < physDevSrc->bmp->width - ySrc ?
                physDevDst->bmp->width - yDst : physDevSrc->bmp->width - ySrc;
    min_height = physDevDst->bmp->height - xDst < physDevSrc->bmp->height - xSrc ?
                 physDevDst->bmp->height - xDst : physDevSrc->bmp->height - xSrc;
    /* copy a scanline at a time */
    for (i = 0; i < min_height; i++)
    {
        physDevDst->funcs->MoveXY( physDevDst->bmp, xDst, i + yDst );
        physDevSrc->funcs->MoveXY( physDevSrc->bmp, xSrc, i + ySrc );
        physDevDst->funcs->Copy( physDevDst->bmp, physDevSrc->bmp, min_width, physDevSrc->convs );
    }
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_AlphaBlend
 */
BOOL DIBDRV_AlphaBlend( DIBDRVPHYSDEV *devDst, INT xDst, INT yDst, INT widthDst, INT heightDst,
                        DIBDRVPHYSDEV *devSrc, INT xSrc, INT ySrc, INT widthSrc, INT heightSrc,
                        BLENDFUNCTION blendfn)
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_BitBlt
 */
BOOL DIBDRV_BitBlt( DIBDRVPHYSDEV *physDevDst, INT xDst, INT yDst,
                    INT width, INT height, DIBDRVPHYSDEV *physDevSrc,
                    INT xSrc, INT ySrc, DWORD rop )
{
    TRACE("stub %p %d %d %d %d %p %d %d %u\n", physDevDst, xDst, yDst, width, height, physDevSrc,
          xSrc, ySrc, rop);
    return DIBDRV_StretchBlt( physDevDst, xDst, yDst, width, height, physDevSrc, xSrc, ySrc,
                              width, height, rop );
}

/***********************************************************************
 *           DIBDRV_PatBlt
 */
BOOL DIBDRV_PatBlt( DIBDRVPHYSDEV *physDev, INT left, INT top, INT width, INT height, DWORD rop )
{
    FIXME("stub\n");
    return TRUE;
}
