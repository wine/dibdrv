/*
 * DIBDRV implementation of GDI driver graphics functions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/***********************************************************************
 *           DIBDRV_Arc
 */
BOOL
DIBDRV_Arc( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
            INT xstart, INT ystart, INT xend, INT yend )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Chord
 */
BOOL
DIBDRV_Chord( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
              INT xstart, INT ystart, INT xend, INT yend )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Ellipse
 */
BOOL
DIBDRV_Ellipse( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom )
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_ExtFloodFill
 */
BOOL
DIBDRV_ExtFloodFill( DIBDRVPHYSDEV *physDev, INT x, INT y, COLORREF color,
                     UINT fillType )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetDCOrgEx
 */
BOOL DIBDRV_GetDCOrgEx( DIBDRVPHYSDEV *physDev, LPPOINT lpp )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_GetPixel
 */
COLORREF DIBDRV_GetPixel( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    BYTE r, g, b;
    TRACE("%p %d %d\n", physDev, x , y);
    physDev->funcs->MoveXY( physDev->bmp, x, y );
    physDev->funcs->ReadColor( physDev->bmp, &r, &g, &b );
    return RGB( r, g, b );
}

/***********************************************************************
 *           DIBDRV_LineTo
 */
BOOL
DIBDRV_LineTo( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_PaintRgn
 */
BOOL
DIBDRV_PaintRgn( DIBDRVPHYSDEV *physDev, HRGN hrgn )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Pie
 */
BOOL
DIBDRV_Pie( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom,
            INT xstart, INT ystart, INT xend, INT yend )
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_Polygon
 */
BOOL
DIBDRV_Polygon( DIBDRVPHYSDEV *physDev, const POINT* pt, INT count )
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_Polyline
 */
BOOL
DIBDRV_Polyline( DIBDRVPHYSDEV *physDev, const POINT* pt, INT count )
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_PolyPolygon
 */
BOOL
DIBDRV_PolyPolygon( DIBDRVPHYSDEV *physDev, const POINT* pt, const INT* counts, UINT polygons)
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_PolyPolyline
 */
BOOL
DIBDRV_PolyPolyline( DIBDRVPHYSDEV *physDev, const POINT* pt, const DWORD* counts,
                     DWORD polylines )
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_Rectangle
 */
BOOL
DIBDRV_Rectangle( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right, INT bottom)
{
    FIXME("stub\n");
    return TRUE;
}

/***********************************************************************
 *           DIBDRV_RoundRect
 */
BOOL
DIBDRV_RoundRect( DIBDRVPHYSDEV *physDev, INT left, INT top, INT right,
                  INT bottom, INT ell_width, INT ell_height )
{
    FIXME("stub\n");
    return TRUE;
}

/**********************************************************************
 *          DIBDRV_SetBkColor
 */
COLORREF
DIBDRV_SetBkColor( DIBDRVPHYSDEV *physDev, COLORREF color )
{
    FIXME("stub\n");
    return color;
}

/***********************************************************************
 *           DIBDRV_SetPixel
 */
COLORREF DIBDRV_SetPixel( DIBDRVPHYSDEV *physDev, INT x, INT y, COLORREF color )
{
    BYTE r, g, b;
    TRACE("%p %d %d %u\n", physDev, x, y, color);
    physDev->funcs->MoveXY( physDev->bmp, x, y );
    physDev->funcs->SetPixel( physDev->bmp, color );
    physDev->funcs->ReadColor( physDev->bmp, &r, &g, &b );
    return RGB( r, g, b );
}

/**********************************************************************
 *          DIBDRV_SetTextColor
 */
COLORREF
DIBDRV_SetTextColor( DIBDRVPHYSDEV *physDev, COLORREF color )
{
    FIXME("stub\n");
    return color;
}

/***********************************************************************
 *           DIBDRV_SetDCOrg
 */
DWORD DIBDRV_SetDCOrg( DIBDRVPHYSDEV *physDev, INT x, INT y )
{
    FIXME("stub\n");
    return 0;
}
