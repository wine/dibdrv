/*
 * DIBDRV depth specific DIB conversion functions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wingdi.h"
#include "wine/debug.h"

#include "dib_render.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/* These functions convert the pixel found in the source address to the requested destination
 * format. The result is stored in the destination address. This is done for the requested
 * consecutive number of pixels. The caller is responsible to correctly traverse scanlines and
 * not cause the conversion routines to go past an end of a scanline.
 *
 * Optimizations can be added when time permits.
 *
 * The "Slow" Conversion Functions:
 * DIB1, DIB4, and DIB8 to any
 * any to DIB1, DIB4, and DIB8
 * DIB16, DIB24, and DIB32 to DIB16
 * DIB16, DIB24, and DIB32 to DIB32
 * They assume the worst case scenario in that we do not have the same color table even for the
 * same bitdepths. They advance one pixel at a time, converting exactly. Therefore, these routines
 * should work in 100% of the time.
 */

static void DIB1_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB1_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB1_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB1_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB1_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB1_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB1_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB1_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB1_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB1_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB1_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB1_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}

static void DIB1_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *d = dst->addr;
        DIB1_ReadColor( src, &d->rgbtRed, &d->rgbtGreen, &d->rgbtBlue );
        DIB1_AdvancePosition( src, 1 );
        DIB24_AdvancePosition( dst, 1 );
    }
}

static void DIB1_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB1_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB1_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB4_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB4_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB4_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB4_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB4_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB4_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB4_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB4_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *d = dst->addr;
        DIB4_ReadColor( src, &d->rgbtRed, &d->rgbtGreen, &d->rgbtBlue );
        DIB4_AdvancePosition( src, 1 );
        DIB24_AdvancePosition( dst, 1 );
    }
}

static void DIB4_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB4_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB4_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

static void DIB8_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB8_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB8_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB8_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB8_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB8_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB8_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB8_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB8_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB8_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB8_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB8_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}


static void DIB8_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *d = dst->addr;
        DIB8_ReadColor( src, &d->rgbtRed, &d->rgbtGreen, &d->rgbtBlue );
        DIB8_AdvancePosition( src, 1 );
        DIB24_AdvancePosition( dst, 1 );
    }
}

static void DIB8_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB8_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB8_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB16_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB16_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB16_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB16_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB16_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB16_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB16_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB16_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *d = dst->addr;
        DIB16_ReadColor( src, &d->rgbtRed, &d->rgbtGreen, &d->rgbtBlue );
        DIB16_AdvancePosition( src, 1 );
        DIB24_AdvancePosition( dst, 1 );
    }
}

static void DIB16_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB16_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB16_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

static void DIB24_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB24_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB24_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB24_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB24_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB24_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB24_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB24_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB24_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB24_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB24_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB24_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}

static void DIB24_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    memcpy( dst->addr, src->addr, sizeof(RGBTRIPLE)*num_pixels );
}

static void DIB24_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB24_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB24_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB1( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB32_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b ) ?
                            *(BYTE*)dst->addr | 1 << dst->shift :
                            *(BYTE*)dst->addr & ~(1 << dst->shift);
        DIB32_AdvancePosition( src, 1 );
        DIB1_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB4( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB32_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = dst->shift ?
                            (*(BYTE*)dst->addr & 0xf0) |
                            DIB_GetNearestIndex( dst, r, g, b ) :
                            (*(BYTE*)dst->addr & 0xf) |
                            (DIB_GetNearestIndex( dst, r, g, b ) << 4);
        DIB32_AdvancePosition( src, 1 );
        DIB4_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB8( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB32_ReadColor( src, &r, &g, &b );
        *(BYTE*)dst->addr = DIB_GetNearestIndex( dst, r, g, b );
        DIB32_AdvancePosition( src, 1 );
        DIB8_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB16( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB32_ReadColor( src, &r, &g, &b );
        *(WORD*)dst->addr = DIB16_Encode( dst, r, g, b );
        DIB32_AdvancePosition( src, 1 );
        DIB16_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB24( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        RGBTRIPLE *d = dst->addr;
        DIB32_ReadColor( src, &d->rgbtRed, &d->rgbtGreen, &d->rgbtBlue );
        DIB32_AdvancePosition( src, 1 );
        DIB24_AdvancePosition( dst, 1 );
    }
}

static void DIB32_ConvertToDIB32( DIBDRVBITMAP *dst, DIBDRVBITMAP *src, int num_pixels )
{
    BYTE r, g, b;
    int i;
    for (i = 0; i < num_pixels; i++)
    {
        DIB32_ReadColor( src, &r, &g, &b );
        *(DWORD*)dst->addr = DIB32_Encode( dst, r, g, b );
        DIB32_AdvancePosition( src, 1 );
        DIB32_AdvancePosition( dst, 1 );
    }
}

const DIBCONVERT DIB1_convs = {
    DIB1_ConvertToDIB1,
    DIB1_ConvertToDIB4,
    DIB1_ConvertToDIB8,
    DIB1_ConvertToDIB16,
    DIB1_ConvertToDIB24,
    DIB1_ConvertToDIB32
};

const DIBCONVERT DIB4_convs = {
    DIB4_ConvertToDIB1,
    DIB4_ConvertToDIB4,
    DIB4_ConvertToDIB8,
    DIB4_ConvertToDIB16,
    DIB4_ConvertToDIB24,
    DIB4_ConvertToDIB32
};

const DIBCONVERT DIB8_convs = {
    DIB8_ConvertToDIB1,
    DIB8_ConvertToDIB4,
    DIB8_ConvertToDIB8,
    DIB8_ConvertToDIB16,
    DIB8_ConvertToDIB24,
    DIB8_ConvertToDIB32
};

const DIBCONVERT DIB16_convs = {
    DIB16_ConvertToDIB1,
    DIB16_ConvertToDIB4,
    DIB16_ConvertToDIB8,
    DIB16_ConvertToDIB16,
    DIB16_ConvertToDIB24,
    DIB16_ConvertToDIB32
};

const DIBCONVERT DIB24_convs = {
    DIB24_ConvertToDIB1,
    DIB24_ConvertToDIB4,
    DIB24_ConvertToDIB8,
    DIB24_ConvertToDIB16,
    DIB24_ConvertToDIB24,
    DIB24_ConvertToDIB32
};

const DIBCONVERT DIB32_convs = {
    DIB32_ConvertToDIB1,
    DIB32_ConvertToDIB4,
    DIB32_ConvertToDIB8,
    DIB32_ConvertToDIB16,
    DIB32_ConvertToDIB24,
    DIB32_ConvertToDIB32
};
