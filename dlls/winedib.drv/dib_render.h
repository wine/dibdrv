/*
 * DIBDRV depth specific DIB rendering functions private header
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WINE_DIBDRV_DIB_RENDER_H
#define __WINE_DIBDRV_DIB_RENDER_H

#include "dibdrv.h"

/* DIB_GetColorTableEntry: returns color components of the requested color table entry. */
static inline void DIB_GetColorTableEntry( DIBDRVBITMAP *bmp, BYTE i, BYTE *r, BYTE *g, BYTE *b )
{
    *r = bmp->color_table[i].rgbRed;
    *g = bmp->color_table[i].rgbGreen;
    *b = bmp->color_table[i].rgbBlue;
}

/* 16/24/32 RGB encoders and decoders */

static inline WORD DIB16_Encode(DIBDRVBITMAP *bmp, BYTE r, BYTE g, BYTE b)
{
    return (r >> bmp->bf_shift[0]) << bmp->bf_pos[0] |
           (g >> bmp->bf_shift[1]) << bmp->bf_pos[1] |
           (b >> bmp->bf_shift[2]) << bmp->bf_pos[2];
}

static inline void DIB16_Decode(DIBDRVBITMAP *bmp, WORD c, BYTE *r, BYTE *g, BYTE *b)
{
    *r = (c & bmp->bitfields[0]) >> bmp->bf_pos[0] << bmp->bf_shift[0];
    *g = (c & bmp->bitfields[1]) >> bmp->bf_pos[1] << bmp->bf_shift[1];
    *b = (c & bmp->bitfields[2]) >> bmp->bf_pos[2] << bmp->bf_shift[2];
    if (bmp->bf_size[0] < 8) *r |= *r >> bmp->bf_size[0];
    if (bmp->bf_size[1] < 8) *g |= *g >> bmp->bf_size[1];
    if (bmp->bf_size[2] < 8) *b |= *b >> bmp->bf_size[2];
}

static inline RGBTRIPLE DIB24_Encode(DIBDRVBITMAP *bmp, BYTE r, BYTE g, BYTE b)
{
    RGBTRIPLE c;
    c.rgbtRed = r;
    c.rgbtGreen = g;
    c.rgbtBlue = b;
    return c;
}

static inline void DIB24_Decode(DIBDRVBITMAP *bmp, RGBTRIPLE c, BYTE *r, BYTE *g, BYTE *b)
{
    *r = c.rgbtRed;
    *g = c.rgbtGreen;
    *b = c.rgbtBlue;
}

static inline DWORD DIB32_Encode(DIBDRVBITMAP *bmp, BYTE r, BYTE g, BYTE b)
{
    return (r >> bmp->bf_shift[0]) << bmp->bf_pos[0] |
           (g >> bmp->bf_shift[1]) << bmp->bf_pos[1] |
           (b >> bmp->bf_shift[2]) << bmp->bf_pos[2];
}

static inline void DIB32_Decode(DIBDRVBITMAP *bmp, DWORD c, BYTE *r, BYTE *g, BYTE *b)
{
    *r = (c & bmp->bitfields[0]) >> bmp->bf_pos[0] << bmp->bf_shift[0];
    *g = (c & bmp->bitfields[1]) >> bmp->bf_pos[1] << bmp->bf_shift[1];
    *b = (c & bmp->bitfields[2]) >> bmp->bf_pos[2] << bmp->bf_shift[2];
    if (bmp->bf_size[0] < 8) *r |= *r >> bmp->bf_size[0];
    if (bmp->bf_size[1] < 8) *g |= *g >> bmp->bf_size[1];
    if (bmp->bf_size[2] < 8) *b |= *b >> bmp->bf_size[2];
}

extern INT DIB_GetNearestIndex( DIBDRVBITMAP *bmp, BYTE r, BYTE g, BYTE b );
extern void DIB1_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB4_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB8_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB16_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB24_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB32_AdvancePosition( DIBDRVBITMAP *bmp, int num_pixels );
extern void DIB1_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );
extern void DIB4_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );
extern void DIB8_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );
extern void DIB16_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );
extern void DIB24_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );
extern void DIB32_ReadColor( DIBDRVBITMAP *bmp, BYTE *r, BYTE *g, BYTE *b );

#endif /* __WINE_DIBDRV_DIB_RENDER_H */
