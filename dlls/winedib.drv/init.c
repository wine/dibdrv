/*
 * DIB driver initialization functions
 *
 * Copyright 2007 Jesse Allen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "config.h"

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wine/debug.h"

#include "dibdrv.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibdrv);

/**********************************************************************
 *           DIBDRV_CreateDC
 */
BOOL DIBDRV_CreateDC( HDC hdc, DIBDRVPHYSDEV **pdev, LPCWSTR driver, LPCWSTR device,
                      LPCWSTR output, const DEVMODEW* initData )
{
    DIBDRVPHYSDEV *physDev;

    physDev = HeapAlloc( GetProcessHeap(), HEAP_ZERO_MEMORY, sizeof(*physDev) );
    if (!physDev) return FALSE;

    *pdev = physDev;
    physDev->hdc = hdc;

    return TRUE;
}

/**********************************************************************
 *           DIBDRV_DeleteDC
 */
BOOL DIBDRV_DeleteDC( DIBDRVPHYSDEV *physDev )
{
    if (physDev->bmp)
    {
        if (physDev->bmp->color_table)
            HeapFree( GetProcessHeap(), 0, physDev->bmp->color_table );
        HeapFree( GetProcessHeap(), 0, physDev->bmp );
    }
    HeapFree( GetProcessHeap(), 0, physDev );
    return TRUE;
}

/**********************************************************************
 *           DIBDRV_ExtEscape
 */
INT DIBDRV_ExtEscape( DIBDRVPHYSDEV *physDev, INT escape, INT in_count, LPCVOID in_data,
                      INT out_count, LPVOID out_data )
{
    FIXME("stub\n");
    return 0;
}

/***********************************************************************
 *           DIBDRV_GetDeviceCaps
 */
INT DIBDRV_GetDeviceCaps( DIBDRVPHYSDEV *physDev, INT cap )
{
    FIXME("stub\n");
    return 0;
}
